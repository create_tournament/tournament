package controllers

import (
	"m/database"
	"m/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

// getTournamentLast : トーナメントの最後をJSONで返却する関数
func getTournamentLast(ctx *gin.Context) {
	var tournament = database.TournamentInsertLast()
	ctx.JSON(200, gin.H{"tournament": tournament})
}

// TournamentList : トーナメント一覧をJSONで返却する関数
func TournamentList(ctx *gin.Context) {
	var tournaments = database.TournamentGetAll()
	ctx.JSON(200, gin.H{"tournaments": tournaments})
}

// ViewRecord : トーナメントIDをもとにレコードをJSONで返却する関数
func ViewRecord(ctx *gin.Context) {
	strTournamentID := ctx.Param("id")
	tournamentID, err := strconv.Atoi(strTournamentID)
	if err != nil {
		panic(err)
	}
	var recordList []models.RecordJSON
	recordJSON := models.RecordJSON{}
	var records = database.RecordGetBytournamentID(tournamentID)
	for _, record := range records {
		recordJSON.ID = int(record.ID)
		recordJSON.TeamName1 = database.TeamGetByID(record.Team1ID).TeamName
		recordJSON.TeamName2 = database.TeamGetByID(record.Team2ID).TeamName
		recordJSON.Team1Score = record.Team1Score
		recordJSON.Team2Score = record.Team2Score
		recordList = append(recordList, recordJSON)
	}
	ctx.JSON(200, gin.H{"record": recordList})
}

// RoundViewJSON : Roundの情報をJSONで返却する関数
func RoundViewJSON(ctx *gin.Context) {
	strTournamentID := ctx.Param("id")
	tournamentID, err := strconv.Atoi(strTournamentID)
	if err != nil {
		panic(err)
	}
	var rounds = viewRound(tournamentID)
	ctx.JSON(200, gin.H{"rounds": rounds})
}
