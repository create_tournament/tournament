package controllers

import (
	"github.com/gin-gonic/gin"
)

// TournamentIndex : index.htmlに接続する関数
func TournamentIndex(ctx *gin.Context) {
	ctx.HTML(200, "index.html", gin.H{})
}

// TournamentListView : tournament_list.htmlに接続する関数
func TournamentListView(ctx *gin.Context) {
	ctx.HTML(200, "tournament_list.html", gin.H{})
}

// TournamentNew : tournament_create.htmlに接続する関数
func TournamentNew(ctx *gin.Context) {
	ctx.HTML(200, "tournament_create.html", gin.H{})
}

// TournamentDetailView : tournament.htmlに接続する関数
func TournamentDetailView(ctx *gin.Context) {
	ctx.HTML(200, "tournament.html", gin.H{})
}
