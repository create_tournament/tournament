package controllers

import (
	"fmt"
	"m/database"
	"m/models"
	"math"
	"math/rand"
	"reflect"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// setRecord : チーム登録の際にあらかじめレコードを作成するメソッド
func setRecord(tournamentID int, teams []models.Team) {
	var exp float64
	var seed int
	var teamIDList []int
	var seedPlace []int
	var placeList []int
	exp = 1.0
	for _, team := range teams {
		teamIDList = append(teamIDList, int(team.ID))
	}
	// 累乗からトーナメントの大きさを測定
	for math.Pow(2, exp) < float64(len(teams)) {
		exp++
	}
	//シャッフル
	rand.Seed(time.Now().UnixNano())
	for i := range teamIDList {
		j := rand.Intn(i + 1)
		teamIDList[i], teamIDList[j] = teamIDList[j], teamIDList[i]
	}

	// シードの数
	seed = int(math.Pow(2, exp) - float64(len(teams)))
	for i := 0; i < int(math.Pow(2, exp)); i++ {
		if i%2 != 0 {
			seedPlace = append(seedPlace, i)
		}
	}

	// 奇数番目をシードにする(チームID 0)
	for i := 0; i < seed; i++ {
		if i%2 == 0 {
			place := seedPlace[0]
			teamIDList = Insert(teamIDList, place, 0)
			seedPlace = seedPlace[1:]
		} else {
			place := seedPlace[len(seedPlace)-1]
			seedPlace = seedPlace[:len(seedPlace)-1]
			placeList = append(placeList, place)
		}
	}
	var placeListlen = len(placeList)
	for i := 0; i < placeListlen; i++ {
		place := placeList[len(placeList)-1]
		placeList = placeList[:len(placeList)-1]
		teamIDList = Insert(teamIDList, place, 0)
	}

	teamIDList, err := Fill(teamIDList, 0, len(teamIDList), int(math.Pow(2, exp+1))-2)
	if err != nil {
		panic(err)
	}
	for i := 0; i < len(teamIDList); i++ {
		var teamID1 int
		var teamID2 int
		teamID1 = teamIDList[i]
		i++
		teamID2 = teamIDList[i]
		database.RecordInsert(tournamentID, teamID1, teamID2)
	}
	setNextID(tournamentID, int(math.Pow(2, exp)))
	defer seedUpdate(tournamentID)
}

// setNextID : 次の対戦IDをレコードに記録するメソッド
func setNextID(tournamentID int, tournamentLarge int) {
	var record = database.RecordGetBytournamentID(tournamentID)
	var nextIDList []int
	for i := tournamentLarge / 2; i < tournamentLarge-1; i++ {
		nextIDList = append(nextIDList, int(record[i].ID))
	}
	var count = 0
	for i := 0; i < len(nextIDList); i++ {
		for j := count; j < count+2; j++ {
			database.RecordNextIDUpdate(int(record[j].ID), nextIDList[i])
		}
		count += 2
	}
}

// viewRound : recordからRoundを整形して作成する関数
func viewRound(tournamentID int) models.Round {
	var recordList = database.RecordGetBytournamentID(tournamentID)
	Games := models.Games{}
	Rounds := models.Rounds{}
	Round := models.Round{}

	var RoundList []models.Rounds
	var exp float64
	exp = 1.0
	for math.Pow(2, exp) < float64(len(recordList)+1) {
		exp++
	}

	for j := int(exp); j > 0; j-- {
		var GameList []models.Games
		for i, record := range recordList {
			Player1 := models.Player1{}
			Player2 := models.Player2{}
			Player1.ID = strconv.Itoa(record.Team1ID)
			Player1.Name = database.TeamGetByID(record.Team1ID).TeamName

			if !IsNil(record.Team1Score) {
				if !IsNil(record.Team2Score) {
					Player1.Winner = Comparison(record.Team1Score, record.Team2Score)
					Player2.Winner = Comparison(record.Team2Score, record.Team1Score)
				}
			}

			Player2.ID = strconv.Itoa(record.Team2ID)
			Player2.Name = database.TeamGetByID(record.Team2ID).TeamName

			Games.ID = int(record.ID)
			Games.Next = record.NextID
			Games.Player1 = Player1
			Games.Player2 = Player2

			GameList = append(GameList, Games)
			if i == int(math.Pow(2, float64(j)))/2-1 {
				break
			}
		}
		Rounds.Games = GameList
		RoundList = append(RoundList, Rounds)
		recordList = recordList[int(math.Pow(2, float64(j)))/2:]
	}
	Round.Rounds = RoundList
	return Round
}

// Comparison : 比較する関数
func Comparison(num1 *int, num2 *int) *bool {
	var result *bool
	result2 := false
	if result == nil {
		result = &result2
	}
	if *num1 > *num2 {
		*result = true
	}
	return result
}

// Insert : int型の配列にiの場所にvalueを格納する関数
func Insert(s []int, i int, value int) []int {
	pos := i
	s = append(s[:pos+1], s[pos:]...)
	s[pos] = value
	return s
}

// Fill : 指定した範囲である値を埋める関数
func Fill(slice []int, val, start, end int) ([]int, error) {
	if len(slice) < start {
		return nil, fmt.Errorf("Error")
	}
	for i := start; i < end; i++ {
		slice = append(slice, val)
	}
	return slice, nil
}

// UpdateRecord : レコードをアップデートする関数
func UpdateRecord(ctx *gin.Context) {
	strRecordID := ctx.PostForm("recordID")
	recordID, err := strconv.Atoi(strRecordID)
	var updateFlag bool
	updateFlag = true
	if err != nil {
		panic(err)
	}
	strTeam1Score := ctx.PostForm("Team1Score")
	team1Score, err := strconv.Atoi(strTeam1Score)
	if err != nil {
		updateFlag = false
	}
	strTeam2Score := ctx.PostForm("Team2Score")
	team2Score, err := strconv.Atoi(strTeam2Score)
	if err != nil {
		updateFlag = false
	}
	if updateFlag {
		database.RecordScoreUpdate(recordID, team1Score, team2Score)
	}
	var record = database.RecordGetByID(recordID)
	defer latestRecord(record.TournamentID)
}

// IsNil : nilかどうか判定するメソッド
func IsNil(value interface{}) bool {
	if (value == nil) || reflect.ValueOf(value).IsNil() {
		return true
	}
	return false
}

// latestRecord : 勝者を次のレコードに登録するメソッド
func latestRecord(tournamentID int) {
	var recordList = database.RecordGetBytournamentID(tournamentID)
	var Player1Winner *bool
	var Player2Winner *bool
	for i, record := range recordList {
		if !IsNil(record.Team1Score) {
			if !IsNil(record.Team2Score) {
				Player1Winner = Comparison(record.Team1Score, record.Team2Score)
				Player2Winner = Comparison(record.Team2Score, record.Team1Score)
				if i%2 == 0 {
					if *Player1Winner {
						database.RecordUpdate(record.NextID, record.Team1ID, 1)
					} else if *Player2Winner {
						database.RecordUpdate(record.NextID, record.Team2ID, 1)
					}
				} else {
					if *Player1Winner {
						database.RecordUpdate(record.NextID, record.Team1ID, 2)
					} else if *Player2Winner {
						database.RecordUpdate(record.NextID, record.Team2ID, 2)
					}
				}
			}
		}
	}
}

// seedUpdate : 初戦がシードの場合トーナメントを進める関数
func seedUpdate(tournamentID int) {
	var recordList = database.RecordGetBytournamentID(tournamentID)
	for i, record := range recordList {
		if i > (len(recordList)+1)/2 {
			break
		}
		if record.Team2ID == 0 {
			if i%2 == 0 {
				database.RecordUpdate(record.NextID, record.Team1ID, 1)
			} else {
				database.RecordUpdate(record.NextID, record.Team1ID, 2)
			}
		} else if record.Team1ID == 0 {
			if i%2 == 0 {
				database.RecordUpdate(record.NextID, record.Team2ID, 1)
			} else {
				database.RecordUpdate(record.NextID, record.Team2ID, 2)
			}
		}
	}
}
