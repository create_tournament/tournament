package controllers

import (
	"m/database"
	"m/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

// TournamentCreate : トーナメントを新規作成する関数
func TournamentCreate(ctx *gin.Context) {
	name := ctx.PostForm("name")
	strNumber := ctx.PostForm("number")
	number, err := strconv.Atoi(strNumber)
	if err != nil {
		panic(err)
	}
	database.TournamentInsert(name, number)
	defer getTournamentLast(ctx)
}

// TeamCreate : トーナメント作成後にチームを登録する関数
func TeamCreate(ctx *gin.Context) {
	var teams []models.Team
	number, err := strconv.Atoi(ctx.PostForm("number"))
	tournamentID, err := strconv.Atoi(ctx.PostForm("tournamentID"))
	if err != nil {
		panic(err)
	}
	for i := 0; i < number; i++ {
		var team models.Team
		istr := strconv.Itoa(i + 1)

		team.TeamName = ctx.PostForm("name" + istr)

		team.TournamentID = tournamentID
		teams = append(teams, team)
	}
	database.TeamInsertMany(teams)

	teams = database.TeamGetByTournamentID(tournamentID)
	defer setRecord(tournamentID, teams)
}

// TeamNameChange : チーム名を変更する関数
func TeamNameChange(ctx *gin.Context) {
	number, err := strconv.Atoi(ctx.PostForm("TeamID"))
	if err != nil {
		panic(err)
	}
	TeamName := ctx.PostForm("TeamName")
	database.TeamUpdate(number, TeamName)
}

// TournamentDelete : トーナメントIDからトーナメントを削除する関数
func TournamentDelete(ctx *gin.Context) {
	tournamentID, err := strconv.Atoi(ctx.PostForm("tournamentID"))
	if err != nil {
		panic("ERROR")
	}
	database.TournamentDeleteByID(tournamentID)
}
