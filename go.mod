module m

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v1.14.5
	github.com/wcl48/valval v0.0.0-20190607031309-ea550ab72393
)
