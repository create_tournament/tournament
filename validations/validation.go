package validations

import (
	"m/models"
	"regexp"

	"github.com/wcl48/valval"
)

// TournamentValidate : 大会モデルのバリデーション関数
func TournamentValidate(tournament models.Tournament) error {
	Validator := valval.Object(valval.M{
		"TournamentName": valval.String(
			valval.MaxLength(20),
			valval.Regexp(regexp.MustCompile(`^[a-zA-Zぁ-んーァ-ンヴー0-9亜-熙]+$`)),
		),
		"Number": valval.Number(
			valval.Min(2),
			valval.Max(64),
		),
	})

	return Validator.Validate(tournament)
}

// TeamValidate : チームモデルのバリデーション関数
func TeamValidate(team models.Team) error {
	Validator := valval.Object(valval.M{
		"TeamName": valval.String(
			valval.MaxLength(20),
			valval.Regexp(regexp.MustCompile(`^[a-z ]+$`)),
		),
		"TournamentID": valval.Number(),
	})

	return Validator.Validate(team)
}

// ScoreValidate : スコアのバリデーション関数
/*
func ScoreValidate() error {
	Validator := valval.Object(valval.M{
		"Team1Score": valval.Number(),
		"Team2Score": valval.Number(),
	})

	return Validator.Validate(record)
}
*/
