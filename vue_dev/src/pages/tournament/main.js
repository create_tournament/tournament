﻿import Vue from "vue";
import Tournament from "./Tournament.vue";
import vuetify from "@/plugins/vuetify";
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false;
Vue.use(VueAxios, axios)

new Vue({
  vuetify,
  render: h => h(Tournament)
}).$mount("#app");
