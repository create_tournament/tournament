﻿import Vue from "vue";
import Team_list from "./Team_list.vue";
import vuetify from "@/plugins/vuetify";

Vue.config.productionTip = false;

new Vue({
  vuetify,
  render: h => h(Team_list)
}).$mount("#app");
