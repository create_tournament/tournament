﻿import Vue from "vue";
import Team_create from "./Team_create.vue";
import vuetify from "@/plugins/vuetify";
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false;
Vue.use(VueAxios, axios)

new Vue({
  vuetify,
  render: h => h(Team_create)
}).$mount("#app");
