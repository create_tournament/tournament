﻿import Vue from "vue";
import Tournament_create from "./Tournament_create.vue";
import vuetify from "@/plugins/vuetify";
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false;
Vue.use(VueAxios, axios)

new Vue({
  vuetify,
  render: h => h(Tournament_create)
}).$mount("#app");
