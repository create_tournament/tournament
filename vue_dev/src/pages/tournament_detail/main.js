﻿import Vue from "vue";
import Tournament_detail from "./Tournament_detail.vue";
import vuetify from "@/plugins/vuetify";

Vue.config.productionTip = false;

new Vue({
  vuetify,
  render: h => h(Tournament_detail)
}).$mount("#app");
