module.exports = {
  outputDir: "../templates", //出力先を変更
  pages: {
    index: {
      entry: "src/pages/index/main.js", // エントリーポイントとなるjs
      template: "public/index.html", // テンプレートのHTML
      filename: "index.html", // build時に出力されるファイル名
      title: "The Tournaments" // タイトル
    },
    team_list: {
      entry: "src/pages/team_list/main.js",
      template: "public/team_list.html",
      filename: "team_list.html"
    },
    tournament_create: {
      entry: "src/pages/tournament_create/main.js",
      template: "public/tournament_create.html",
      filename: "tournament_create.html",
      title: "トーナメント作成|The Tournaments"
    },
    tournament_detail: {
      entry: "src/pages/tournament_detail/main.js",
      template: "public/tournament_detail.html",
      filename: "tournament_detail.html"
    },
    tournament_list: {
      entry: "src/pages/tournament_list/main.js",
      template: "public/tournament_list.html",
      filename: "tournament_list.html",
      title: "トーナメントリスト|The Tournaments"
    },
    tournament: {
      entry: "src/pages/tournament/main.js",
      template: "public/tournament.html",
      filename: "tournament.html",
      title: "トーナメント|The Tournaments"
    }
  },
  transpileDependencies: ["vuetify"]
};
