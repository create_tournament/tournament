# このフォルダについて
このフォルダでvueを開発します

## フォルダ構成
- node_module  
言わずもがな  

- public  
大元となるテンプレートhtmlファイル  
ページ一枚に対して一枚必要  

- src  
vueの大元のフォルダ  
各ページmain.jsとvueでワンセット

- vue.config.js
このファイルで呼び出すルールを設定  
