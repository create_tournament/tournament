package database

import (
	"m/models"

	_ "github.com/mattn/go-sqlite3"

	"github.com/jinzhu/gorm"
)

func init() {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed migration : record_info")
	}
	db.AutoMigrate(&models.Record{})
	defer db.Close()
}

// RecordInsert : record_infoテーブルに登録する関数
func RecordInsert(tournamentID int, team1ID int, team2ID int) {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed insert : record_info")
	}
	db.Create(&models.Record{TournamentID: tournamentID, Team1ID: team1ID, Team2ID: team2ID})
	defer db.Close()
}

//RecordGetAll : record_infoテーブルのデータを全取得する関数
func RecordGetAll() []models.Record {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed select All : record_info")
	}
	var records []models.Record
	db.Order("created_at desc").Find(&records)
	db.Close()
	return records
}

//RecordGetByID : record_infoテーブルのデータからidをキーに取得する関数
func RecordGetByID(id int) models.Record {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed select by ID : record_info")
	}
	var record models.Record
	db.First(&record, id)
	db.Close()
	return record
}

// RecordUpdate : record_infoテーブルのスコアをupdateする関数
func RecordUpdate(id int, teamID int, number int) {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed update : record_info")
	}
	var record models.Record
	db.First(&record, id)
	if number == 1 {
		record.Team1ID = teamID
	} else if number == 2 {
		record.Team2ID = teamID
	}
	db.Save(&record)
	db.Close()
}

// RecordScoreUpdate : record_infoテーブルをupdateする関数
func RecordScoreUpdate(id int, team1Score int, team2Score int) {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed update : record_info")
	}
	var record models.Record
	db.First(&record, id)
	record.Team1Score = &team1Score
	record.Team2Score = &team2Score
	db.Save(&record)
	db.Close()
}

// RecordNextIDUpdate : record_infoテーブルのnextIDをupdateする関数
func RecordNextIDUpdate(id int, nextID int) {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed update : record_info")
	}
	var record models.Record
	db.First(&record, id)
	record.NextID = nextID
	db.Save(&record)
	db.Close()
}

// RecordDeleteByID : record_infoテーブルのデータからidをキーに削除する関数
func RecordDeleteByID(id int) {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed delete by ID : record_info")
	}
	var record models.Record
	db.First(&record, id)
	db.Delete(&record)
	db.Close()
}

// RecordGetBytournamentID : record_infoテーブルのデータからtournamentIDをキーにrecordを取得する関数
func RecordGetBytournamentID(tournamentID int) []models.Record {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed Get by tournamentID : record_info")
	}
	var records []models.Record
	db.Find(&records, "tournament_id = ?", tournamentID)
	db.Close()
	return records
}

// RecordGetByTeamID : record_infoテーブルのデータからtournamentIDとteamIDをキーにrecordを取得する関数
func RecordGetByTeamID(tournamentID int, team1ID int, team2ID int) models.Record {
	db, err := gorm.Open("sqlite3", "record_info.sqlite3")
	if err != nil {
		panic("failed Get by tournamentID and teamID : record_info")
	}
	var record models.Record
	db.Find(&record, "tournament_id = ? and team1_id = ? and team2_id = ?", tournamentID, team1ID, team2ID)
	db.Close()
	return record
}
