package database

import (
	"fmt"
	"m/models"
	"m/validations"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"github.com/wcl48/valval"
)

func init() {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed migration : tournament_info")
	} else {
		fmt.Println("DB Open : tournament_info")
	}
	db.AutoMigrate(&models.Tournament{})
	defer db.Close()
}

// TournamentInsert : tournament_infoテーブルに登録する関数
func TournamentInsert(name string, number int) models.Tournament {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed insert : tournament_info")
	}
	Tournament := models.Tournament{TournamentName: name, Number: number}
	if err := validations.TournamentValidate(Tournament); err != nil {
		var ErrMsg string
		errs := valval.Errors(err)
		for _, errInfo := range errs {
			ErrMsg += fmt.Sprint(errInfo.Error)
		}
		panic(ErrMsg)
	}
	db.Create(&Tournament)
	var tournament models.Tournament
	db.Last(&Tournament)
	defer db.Close()
	return tournament
}

//TournamentGetAll : tournament_infoテーブルのデータを全取得する関数
func TournamentGetAll() []models.Tournament {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed select All : tournament_info")
	}
	var tournaments []models.Tournament
	db.Order("ID asc").Find(&tournaments)
	db.Close()
	return tournaments
}

//TournamentGetByID : tournament_infoテーブルのデータからidをキーに取得する関数
func TournamentGetByID(id int) models.Tournament {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed select by ID : tournament_info")
	}
	var tournament models.Tournament
	db.First(&tournament, id)
	db.Close()
	return tournament
}

// TournamentUpdate : tournament_infoテーブルのデータをupdateする関数
func TournamentUpdate(id int, name string, number int) {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed update : tournament_info")
	}
	var tournament models.Tournament
	db.First(&tournament, id)
	tournament.TournamentName = name
	tournament.Number = number
	db.Save(&tournament)
	db.Close()
}

// TournamentDeleteByID : tournament_infoテーブルのデータからidをキーに削除する関数
func TournamentDeleteByID(id int) {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed delete by ID : tournament_info")
	}
	var tournament models.Tournament
	db.First(&tournament, id)
	db.Delete(&tournament)
	db.Close()
}

// TournamentInsertLast : tournament_infoテーブルに登録された最後のデータを取得する関数
func TournamentInsertLast() models.Tournament {
	db, err := gorm.Open("sqlite3", "tournament_info.sqlite3")
	if err != nil {
		panic("failed select by ID : tournament_info")
	}
	var tournament models.Tournament
	db.Last(&tournament)
	db.Close()
	return tournament
}
