package database

import (
	"fmt"
	"m/models"

	_ "github.com/mattn/go-sqlite3"

	"github.com/jinzhu/gorm"
)

func init() {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed migration : team_info")
	} else {
		fmt.Println("DB Open : team_info")
	}
	db.AutoMigrate(&models.Team{})
	defer db.Close()
}

// TeamInsert : team_infoテーブルに登録する関数
func TeamInsert(name string, tournamentID int) {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed insert : team_info")
	}
	db.Create(&models.Team{TeamName: name, TournamentID: tournamentID})
	defer db.Close()
}

// TeamInsertMany : team_infoテーブルに複数のデータを登録する関数
func TeamInsertMany(teams []models.Team) {
	for _, teamData := range teams {
		TeamInsert(teamData.TeamName, teamData.TournamentID)
	}
}

// TeamGetAll : team_infoテーブルのデータを全取得する関数
func TeamGetAll() []models.Team {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed select All : team_info")
	}
	var teams []models.Team
	db.Order("created_at desc").Find(&teams)
	db.Close()
	fmt.Print(teams)
	return teams
}

// TeamGetByID : team_infoテーブルのデータからidをキーに取得する関数
func TeamGetByID(id int) models.Team {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed select by ID : team_info")
	}
	var team models.Team
	db.First(&team, id)
	db.Close()
	return team
}

// TeamUpdate : team_infoテーブルのデータをupdateする関数
func TeamUpdate(id int, name string) {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed update : team_info")
	}
	var team models.Team
	db.First(&team, id)
	team.TeamName = name
	db.Save(&team)
	db.Close()
}

// TeamDeleteByID : team_infoテーブルのデータからidをキーに削除する関数
func TeamDeleteByID(id int) {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed delete by ID : team_info")
	}
	var team models.Team
	db.First(&team, id)
	db.Delete(&team)
	db.Close()
}

// TeamGetByTournamentID : team_infoテーブルのデータからtournamentIDをキーに一覧を取得する関数
func TeamGetByTournamentID(tournamentID int) []models.Team {
	db, err := gorm.Open("sqlite3", "team_info.sqlite3")
	if err != nil {
		panic("failed Get by tournamentID : team_info")
	}
	var teams []models.Team
	db.Find(&teams, "tournament_id = ?", tournamentID)
	db.Close()
	return teams
}
