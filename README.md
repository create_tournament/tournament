# tournament

Go言語でトーナメント表を作成するアプリを作る

# ディレクトリ構成
### Go言語部分

├─controllers  
├─database  
├─models  
├─templates　ビルドしたVueファイルを格納   
│  ├─css  
│  └─js  
├─validations  

### Vue.js部分

└─vue_dev  
&ensp;&emsp;├─node_modules  
&ensp;&emsp;├─public  
&ensp;&emsp;└─src  
&emsp;&emsp;&emsp;├─components  共通部品  
&emsp;&emsp;&emsp;├─pages  各ページ格納  
&emsp;&emsp;&emsp;│  ├─index  
&emsp;&emsp;&emsp;│  ├─team_create  
&emsp;&emsp;&emsp;│  ├─team_list  
&emsp;&emsp;&emsp;│  ├─tournament  
&emsp;&emsp;&emsp;│  ├─tournament_create  
&emsp;&emsp;&emsp;│  ├─tournament_detail  
&emsp;&emsp;&emsp;│  └─tournament_list  
&emsp;&emsp;&emsp;└─plugins  


# 開発方法
### 実際に起動させるには
`go run main.go`  

### Vueをビルドし静的化
`npm run build`  
自動的にtemplateに上書きするよう設定済み  

# テーブル設計
### Team
| チーム名 | トーナメントID |
| ------ | ------ |
| TeamName | TournamentID |

### Tournament
| 大会名 | 参加チーム数 |
| ------ | ------ |
| TournamentName | Number |

### Record
| トーナメントID | チーム1ID | チーム2ID | チーム1スコア | チーム2スコア | 次の対戦レコード |
| ------ | ------ | ------ | ------ | ------ | ------ |
| TournamentID | Team1ID | Team2ID | Team1Score | Team2Score | NextID |
