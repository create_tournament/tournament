package models

// Round : ラウンドモデル。この中に戦績データを格納し、jsonで返却する。
type Round struct {
	Rounds []Rounds `json:"rounds"`
}

// Player1 : プレイヤー1モデル。
type Player1 struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Winner *bool   `json:"winner"`
}

// Player2 : プレイヤー2モデル。
type Player2 struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Winner *bool   `json:"winner"`
}

// Games : ゲームモデル。
type Games struct {
	ID      int     `json:"id"`
	Next    int     `json:"next"`
	Player1 Player1 `json:"player1"`
	Player2 Player2 `json:"player2"`
}

// Rounds : ラウンドモデル。ゲームを格納する。
type Rounds struct {
	Games []Games `json:"games"`
}
