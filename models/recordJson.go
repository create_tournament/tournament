package models

import (
	"github.com/jinzhu/gorm"
)

// RecordJSON : レコードのモデル。レコードをjsonで整形ために使用する。
type RecordJSON struct {
	gorm.Model
	ID         int    // ゲームID
	TeamName1  string // チーム1名前
	TeamName2  string // チーム2名前
	Team1Score *int   // チーム1スコア
	Team2Score *int   // チーム2スコア
}
