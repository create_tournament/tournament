package models

import (
	"github.com/jinzhu/gorm"
)

// Record : レコードのモデル。record_infoテーブルへのCRUDに使用する。
type Record struct {
	gorm.Model
	TournamentID int  // トーナメントID
	Team1ID      int  // チーム1ID
	Team2ID      int  // チーム2ID
	Team1Score   *int // チーム1スコア
	Team2Score   *int // チーム2スコア
	NextID       int  // 次の対戦レコード
}
