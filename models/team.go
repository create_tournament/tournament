package models

import (
	"github.com/jinzhu/gorm"
)

// Team : チームのモデル。team_infoテーブルへのCRUDに使用する。
type Team struct {
	gorm.Model
	TeamName     string // チーム名
	TournamentID int    // トーナメントID
}
