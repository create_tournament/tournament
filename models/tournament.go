package models

import (
	"github.com/jinzhu/gorm"
)

// Tournament : 大会のモデル。tournament_infoテーブルへのCRUDに使用する。
type Tournament struct {
	gorm.Model
	TournamentName string // 大会名
	Number         int    // 参加チーム数
}
