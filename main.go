package main

import (
	"m/controllers"

	"github.com/gin-gonic/gin"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("templates/*.html")
	router.Static("/js", "templates/js")
	router.Static("/css", "templates/css")
	router.StaticFile("/favicon.ico", "templates/favicon.ico")

	// トップページ
	router.GET("/", controllers.TournamentIndex)

	// トーナメント新規作成ページ
	router.GET("/tournament/new", controllers.TournamentNew)

	// トーナメント一覧ページ
	router.GET("/tournament", controllers.TournamentListView)

	// router.GET("/team/new", controllers.TeamNew)
	router.GET("/tournament/detail/:id", controllers.TournamentDetailView)

	// トーナメントグループ
	tournament := router.Group("api/tournament")
	{
		// トーナメント新規作成
		tournament.POST("/new", controllers.TournamentCreate)

		// トーナメント一覧取得
		tournament.GET("/list", controllers.TournamentList)

		// トーナメント削除
		tournament.POST("/delete", controllers.TournamentDelete)
	}

	// チームグループ
	team := router.Group("api/team")
	{
		// チーム新規作成
		team.POST("/new", controllers.TeamCreate)

		// チーム名変更
		team.POST("/nameChange", controllers.TeamNameChange)
	}

	// レコードグループ
	round := router.Group("api/round")
	{
		round.GET("/:id", controllers.RoundViewJSON)
	}

	// レコードグループ
	record := router.Group("api/record")
	{
		record.GET("/:id", controllers.ViewRecord)

		// レコードアップデート
		record.POST("/update", controllers.UpdateRecord)
	}

	router.Run()
}
